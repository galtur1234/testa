import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection = this.db.collection('students');

  public deleteStudent(id:string){
    this.db.doc(`students/${id}`).delete();
  }

    addStudent(name:string, mathematics:number, psychometric:number, payment:boolean, result:string, email:string, timestamp:number){
        const student = {name:name, mathematics:mathematics, psychometric:psychometric, payment:payment, result:result, email:email, timestamp:timestamp};
         this.studentsCollection.add(student);
       }

  getStudent():Observable<any[]>{
        this.studentsCollection = this.db.collection(`students`,
        ref => ref.orderBy('timestamp','desc'));    return this.studentsCollection.snapshotChanges()
  }

  constructor(private db:AngularFirestore) { }
}
