import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  private url = "https://46sbrz6cr8.execute-api.us-east-1.amazonaws.com/beta";


  predict(mathematics:number, psychometric:number, payment:boolean){
    let json = {'students':[
      {'mathematics':mathematics, 'psychometric':psychometric, 'payment':payment}
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;
      })
    )
  }

  constructor(private http:HttpClient) { }
}
