// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDRvOD98f2KhlXwQFP43vez3LlwpGBggUk",
    authDomain: "testa-cb9ec.firebaseapp.com",
    projectId: "testa-cb9ec",
    storageBucket: "testa-cb9ec.appspot.com",
    messagingSenderId: "692620912708",
    appId: "1:692620912708:web:e0187e8dedd9e7b986b411"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
